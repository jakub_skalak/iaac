#include "SobelFilter.h"

#include "opencv2/core/core.hpp"
#include "opencv2/core/ocl.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS
#include <CL/cl.h>
#include <iostream>
#include <fstream>
#include <chrono>

using namespace std;
using namespace std::chrono;
using namespace cv;
using namespace std;

#include <opencv2/core/ocl.hpp>


SobelFilter::SobelFilter()
{

	
}

SobelFilter::SobelFilter(ResultsManager* resultsManager)
{
	resultsManager = resultsManager;
}

//https://stackoverflow.com/questions/20473352/is-conversion-to-gray-scale-a-necessary-step-in-image-preprocessing
void SobelFilter::compute_cpu_opencv(Measurement* measurement) // zamienic na do_measurements_on_cpu
{
	cv::ocl::setUseOpenCL(false);

	Mat image;
	image = imread(measurement->get_path_to_image(), CV_LOAD_IMAGE_GRAYSCALE);

	Mat sobelx, sobely, sobel;

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	cv::Sobel(image, sobelx, CV_8U, 1, 0);
	cv::Sobel(image, sobely, CV_8U, 0, 1);

	cv::bitwise_or(sobelx, sobely, sobel);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	measurement->save_computation_time(duration_cast<nanoseconds>(t2 - t1).count() * 1e-6);
}

void SobelFilter::compute_gpu_opencv(Measurement* measurement) // zamienic na do_measurements_on_cpu
{
	cv::ocl::setUseOpenCL(true);
	std::cout << "use open cl " << cv::ocl::useOpenCL() << std::endl;

	//std::cout << getBuildInformation();

	UMat src1;
	//src1 = imread(measurement->get_path_to_image(), CV_LOAD_IMAGE_COLOR);
	//UMat src1;
	/*	std::chrono::time_point<std::chrono::system_clock> start_s, end_se;
	start_s = std::chrono::system_clock::now();
	imread(measurement->get_path_to_image(), CV_LOAD_IMAGE_COLOR).copyTo(src1);
	end_se = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_time = end_se - start_s;
	std::cout << elapsed_time.count();
	*/


	Mat asd;
	asd = imread(measurement->get_path_to_image(), CV_LOAD_IMAGE_COLOR);
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	asd.copyTo(src1);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	auto duration = duration_cast<milliseconds>(t2 - t1).count();
	std::cout << duration << std::endl;
	
/*	namedWindow("Original image", CV_WINDOW_AUTOSIZE);
	imshow("Original image", src1);*/

	UMat grey;
	cvtColor(src1, grey, CV_BGR2GRAY);
	//https://stackoverflow.com/questions/20473352/is-conversion-to-gray-scale-a-necessary-step-in-image-preprocessing


	UMat sobelx, sobely, sobel;
	std::chrono::time_point<std::chrono::system_clock> start_gd, end_gd;

	start_gd = std::chrono::system_clock::now();
	//Sobel(grey, sobelx, CV_32F, 1, 0);
	end_gd = std::chrono::system_clock::now();

	int erosion_size = 6;
	Mat element = getStructuringElement(cv::MORPH_CROSS,
		cv::Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		cv::Point(erosion_size, erosion_size));

/*	high_resolution_clock::time_point z1 = high_resolution_clock::now();
	//Sobel(grey, sobelx, CV_32F, 1, 0);
	erode(grey, sobelx, element);
	high_resolution_clock::time_point z2 = high_resolution_clock::now();

	auto duration1 = duration_cast<milliseconds>(z2 - z1).count();
	std::cout << duration1 << std::endl;
	*/
	double total_time = 0.0;
	double t = (double)cv::getTickCount();
	cv::Sobel(grey, sobelx, CV_32F, 1, 0);
	cv::Sobel(grey, sobely, CV_32F, 0, 1);

	cv::bitwise_or(sobelx, sobely, sobel);
	t = ((double)cv::getTickCount() - t) / cv::getTickFrequency();
	total_time += t;
	std::cout << "Times passed in seconds: " << t << " ; FPS: " << (1 / t) << std::endl;


	//measurement->save_computation_time(end_gd - start_gd);


	double minVal, maxVal;
	minMaxLoc(sobelx, &minVal, &maxVal); //find minimum and maximum intensities
	cout << "minVal : " << minVal << endl << "maxVal : " << maxVal << endl;

	UMat draw;
	sobelx.convertTo(draw, CV_8U);

	high_resolution_clock::time_point e1 = high_resolution_clock::now();
	Mat draw1 = draw.getMat(ACCESS_READ);
	high_resolution_clock::time_point e2 = high_resolution_clock::now();

	auto duration2 = duration_cast<milliseconds>(e2 - e1).count();
	std::cout << duration2 << std::endl;

/*
	namedWindow("image", CV_WINDOW_AUTOSIZE);
	imshow("image", draw1);
	draw1.release();*/



	waitKey(0);


}

const char *getErrorString(cl_int error)
{
	switch (error) {
		// run-time and JIT compiler errors
	case 0: return "CL_SUCCESS";
	case -1: return "CL_DEVICE_NOT_FOUND";
	case -2: return "CL_DEVICE_NOT_AVAILABLE";
	case -3: return "CL_COMPILER_NOT_AVAILABLE";
	case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
	case -5: return "CL_OUT_OF_RESOURCES";
	case -6: return "CL_OUT_OF_HOST_MEMORY";
	case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
	case -8: return "CL_MEM_COPY_OVERLAP";
	case -9: return "CL_IMAGE_FORMAT_MISMATCH";
	case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
	case -11: return "CL_BUILD_PROGRAM_FAILURE";
	case -12: return "CL_MAP_FAILURE";
	case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
	case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
	case -15: return "CL_COMPILE_PROGRAM_FAILURE";
	case -16: return "CL_LINKER_NOT_AVAILABLE";
	case -17: return "CL_LINK_PROGRAM_FAILURE";
	case -18: return "CL_DEVICE_PARTITION_FAILED";
	case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

		// compile-time errors
	case -30: return "CL_INVALID_VALUE";
	case -31: return "CL_INVALID_DEVICE_TYPE";
	case -32: return "CL_INVALID_PLATFORM";
	case -33: return "CL_INVALID_DEVICE";
	case -34: return "CL_INVALID_CONTEXT";
	case -35: return "CL_INVALID_QUEUE_PROPERTIES";
	case -36: return "CL_INVALID_COMMAND_QUEUE";
	case -37: return "CL_INVALID_HOST_PTR";
	case -38: return "CL_INVALID_MEM_OBJECT";
	case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
	case -40: return "CL_INVALID_IMAGE_SIZE";
	case -41: return "CL_INVALID_SAMPLER";
	case -42: return "CL_INVALID_BINARY";
	case -43: return "CL_INVALID_BUILD_OPTIONS";
	case -44: return "CL_INVALID_PROGRAM";
	case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
	case -46: return "CL_INVALID_KERNEL_NAME";
	case -47: return "CL_INVALID_KERNEL_DEFINITION";
	case -48: return "CL_INVALID_KERNEL";
	case -49: return "CL_INVALID_ARG_INDEX";
	case -50: return "CL_INVALID_ARG_VALUE";
	case -51: return "CL_INVALID_ARG_SIZE";
	case -52: return "CL_INVALID_KERNEL_ARGS";
	case -53: return "CL_INVALID_WORK_DIMENSION";
	case -54: return "CL_INVALID_WORK_GROUP_SIZE";
	case -55: return "CL_INVALID_WORK_ITEM_SIZE";
	case -56: return "CL_INVALID_GLOBAL_OFFSET";
	case -57: return "CL_INVALID_EVENT_WAIT_LIST";
	case -58: return "CL_INVALID_EVENT";
	case -59: return "CL_INVALID_OPERATION";
	case -60: return "CL_INVALID_GL_OBJECT";
	case -61: return "CL_INVALID_BUFFER_SIZE";
	case -62: return "CL_INVALID_MIP_LEVEL";
	case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
	case -64: return "CL_INVALID_PROPERTY";
	case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
	case -66: return "CL_INVALID_COMPILER_OPTIONS";
	case -67: return "CL_INVALID_LINKER_OPTIONS";
	case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

		// extension errors
	case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
	case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
	case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
	case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
	case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
	case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
	default: return "Unknown OpenCL error";
	}
}

double get_event_exec_time(cl_event event)
{
	cl_ulong start_time, end_time;
	/*Get start device counter for the event*/
	clGetEventProfilingInfo(event,
		CL_PROFILING_COMMAND_START,
		sizeof(cl_ulong),
		&start_time,
		NULL);
	/*Get end device counter for the event*/
	clGetEventProfilingInfo(event,
		CL_PROFILING_COMMAND_END,
		sizeof(cl_ulong),
		&end_time,
		NULL);
	/*Convert the counter values to mili seconds*/
	double total_time = (end_time - start_time) * 1e-6;
	return total_time;
}

//http://cis.k.hosei.ac.jp/~wakahara/sobel.c
void SobelFilter::compute_cpu_own(Measurement* measurement) {
	cv::ocl::setUseOpenCL(false);

	Mat image, dst;
	image = imread(measurement->get_path_to_image(), IMREAD_GRAYSCALE);
	dst = Mat(image.rows, image.cols, CV_8U);

	int weight[3][3] = { { -1,  0,  1 },
	{ -2,  0,  2 },
	{ -1,  0,  1 } };
	double pixel_value;
	double min, max;
	int x, y, i, j;  /* Loop variable */

					 /* Maximum values calculation after filtering*/
	min = DBL_MAX;
	max = -DBL_MAX;
	/* Initialization of image2[y][x] */
	for (y = 0; y < image.rows; y++) {
		for (x = 0; x < image.cols; x++) {
			dst.at<uchar>(y, x) = 0;
		}
	}

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	for (y = 1; y < image.rows - 1; y++) {
		for (x = 1; x < image.cols - 1; x++) {
			pixel_value = 0.0;
			for (j = -1; j <= 1; j++) {
				for (i = -1; i <= 1; i++) {
					pixel_value += weight[j + 1][i + 1] * image.at<uchar>(y + j, x + i);
				}
			}
			if (pixel_value < min) min = pixel_value;
			if (pixel_value > max) max = pixel_value;
		}
	}

	/* Generation of image2 after linear transformtion */
	for (y = 1; y < image.rows - 1; y++) {
		for (x = 1; x < image.cols - 1; x++) {
			pixel_value = 0.0;
			for (j = -1; j <= 1; j++) {
				for (i = -1; i <= 1; i++) {
					pixel_value += weight[j + 1][i + 1] * image.at<uchar>(y + j, x + i);
				}
			}
			pixel_value = 255 * (pixel_value - min) / (max - min);
			dst.at<uchar>(y, x) = (unsigned char)pixel_value;
		}
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	measurement->save_computation_time(duration_cast<nanoseconds>(t2 - t1).count() * 1e-6);
}

void SobelFilter::compute_gpu_opencl(Measurement* measurement) {
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	cl_platform_id platform_id = NULL;
	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;
	cl_int ret;
	int errNum;

	std::fstream kernelFile("sobel.cl");
	std::string content(
		(std::istreambuf_iterator<char>(kernelFile)),
		std::istreambuf_iterator<char>()
	);

	size_t csize = content.size();
	const char* kernelCharArray = new char[content.size()];
	kernelCharArray = content.c_str();

	/* Get Platform and Device Info */
	ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
	ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices);

	/* Create OpenCL context */
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);

	/* Create Command Queue */
	command_queue = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &ret);

	cv::Mat image = cv::imread(measurement->get_path_to_image(), CV_LOAD_IMAGE_GRAYSCALE);
	cv::Mat out;
	out = cv::Mat(image.rows, image.cols, CV_8U);
	char *bufferOut = reinterpret_cast<char *>(image.data);

	int width = image.cols;
	int height = image.rows;

	char *buffer = reinterpret_cast<char *>(image.data);

	cl_image_format clImageFormat;
	clImageFormat.image_channel_order = CL_R;
	clImageFormat.image_channel_data_type = CL_UNORM_INT8;

	cl_mem clImage = clCreateImage2D(context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		&clImageFormat,
		width,
		height,
		0,
		buffer,
		&errNum);

	size_t origin[] = { 0,0,0 };
	size_t region[] = { width, height, 1 };

	cl_event memSendEvent;
	ret = clEnqueueWriteImage(command_queue,
		clImage,
		CL_TRUE,
		origin,
		region,
		0,
		0,
		buffer,
		0, 0, &memSendEvent);

		clWaitForEvents(1, &memSendEvent);
		clFinish(command_queue);
		measurement->save_send_to_gpu_time(get_event_exec_time(memSendEvent));

	cl_mem clImageDest = clCreateImage2D(context,
		CL_MEM_WRITE_ONLY,
		&clImageFormat,
		width,
		height,
		0,
		bufferOut,
		&errNum);

	if (errNum != CL_SUCCESS)
		std::cerr << getErrorString(errNum) << std::endl;

	program = clCreateProgramWithSource(context, 1, (const char **)&kernelCharArray,
		(const size_t *)&csize, &ret);

	/* Build Kernel Program */
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);

	/* Create OpenCL Kernel */
	kernel = clCreateKernel(program, "sobel", &ret);

	/* Set OpenCL Kernel Parameters */
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&clImage);
	ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&clImageDest);
	int val = 3;
	ret = clSetKernelArg(kernel, 2, sizeof(int), (void *)&val);

	/* Execute OpenCL Kernel */
	ret = clEnqueueTask(command_queue, kernel, 0, NULL, NULL);

	cl_event execEvent;

	size_t localThreads[] = { measurement->get_work_items_size(),measurement->get_work_items_size(), 1};

	ret = clEnqueueNDRangeKernel(command_queue,
		kernel,
		3,
		0,
		region,
		localThreads,
		0, 0, &execEvent);
	if (ret != CL_SUCCESS)
		std::cerr << getErrorString(ret) << std::endl;

	clWaitForEvents(1, &execEvent);
	clFinish(command_queue);
	measurement->save_computation_time(get_event_exec_time(execEvent));

	cl_event memDownloadEvent;
	ret = clEnqueueReadImage(command_queue,
		clImageDest,
		CL_TRUE,
		origin,
		region,
		0,
		0,
		bufferOut,
		0, 0, &memDownloadEvent);

	clWaitForEvents(1, &memDownloadEvent);
	clFinish(command_queue);
	measurement->save_download_time(get_event_exec_time(memDownloadEvent));

	if (ret != CL_SUCCESS)
		std::cerr << getErrorString(ret) << std::endl;

	//cv::Mat TempMat = cv::Mat(height, width, CV_8U, bufferOut);

	ret = clFlush(command_queue);
	ret = clFinish(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseMemObject(clImageDest);
	ret = clReleaseMemObject(clImage);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);
	if (ret != CL_SUCCESS)
		std::cerr << getErrorString(ret) << std::endl;

	image.release();
	out.release();
}

SobelFilter::~SobelFilter()
{
}
