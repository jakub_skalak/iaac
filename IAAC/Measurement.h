#pragma once

#include<string>
#include<chrono>


class Measurement
{
public:
	enum ComputationUnit { CPU, GPU };
	Measurement();
	void set_paremeters(std::string alghoritm_name, ComputationUnit computationUnit, std::string path_to_image);
	void print_parameters();
	void save_computation_time(double elapsed_time);
	void save_send_to_gpu_time(double elapsed_time);
	void save_download_time(double elapsed_time);
	void set_work_items_size(int size);
	int get_work_items_size();
	std::string get_path_to_image();
	void save_results_to_file();
	~Measurement();

private:
	std::string alghoritm_name;
	ComputationUnit computationUnit;
	std::string path_to_image;
	double measurement_time;
	double send_to_gpu_time;
	double download_from_gpu_time;
	int work_items_in_work_group;
};

