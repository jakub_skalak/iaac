#pragma once

#include <CL/cl.h>
#include <chrono>

class OpenCLHelper {
public:
	// Returns string with associated with error code
	const char *get_error_string(cl_int error);
	// Calculate duration of event and returns in ms
	double get_event_exec_time(cl_event event);
};

