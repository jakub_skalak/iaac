﻿#include <CL/cl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <sstream>

#include "Main.h"
#include "SobelFilter.h"
#include "ResultsManager.h"
#include "Measurement.h"
#include "Threshold.h"
#include "OpenCLHelper.h"
#include "MedianFilter.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/ocl.hpp>


int main(int argc, char** argv) {
	ResultsManager resultsManager = ResultsManager();
	OpenCLHelper openCLhelper = OpenCLHelper();
	SobelFilter sobelObject = SobelFilter(resultsManager, openCLhelper);
	Threshold thresholdObject = Threshold(resultsManager, openCLhelper);
	MedianFilter medianFilter = MedianFilter(resultsManager, openCLhelper);
	
	std::string path_to_image = "../Data/4kfree.jpg";

	std::string userInput;
	int key;

	do {
		std::cout << "===================================================\n";
		std::cout << "Image processing algorithms performance analyser\n";
		std::cout << "version 1.0 January 2018\n";
		std::cout << "Jakub Skalak - jakub.skalak@gmail.com\n";
		std::cout << "===================================================\n";
		std::cout << "[1] - Set image\n";
		std::cout << "===================================================\n";
		std::cout << "[2] - Sobel on CPU OpenCV\n";
		std::cout << "[3] - Sobel on CPU own implementation\n";
		std::cout << "[4] - Sobel on GPU OpenCL\n";
		std::cout << "[5] - Sobel real-time simulation on CPU OpenCV\n";
		std::cout << "[6] - Sobel real-time simulation on GPU OpenCL\n";
		std::cout << "===================================================\n";
		std::cout << "[7] - Threshold on CPU OpenCV\n";
		std::cout << "[8] - Threshold on CPU own implementation\n";
		std::cout << "[9] - Threshold on GPU OpenCL\n";
		std::cout << "===================================================\n";
		std::cout << "[10] - Median on CPU OpenCV\n";
		std::cout << "[11] - Median on CPU own implementation\n";
		std::cout << "[12] - Median on GPU OpenCL\n";
		std::cout << "===================================================\n";
		std::cout << "[13] - Print results\n";
		std::cout << "[14] - Save all results to files\n";
		std::cout << "[0] - Exit\n";

		std::getline(std::cin, userInput);
		key = std::stoi(userInput);
		switch (key)
		{
		case 1:
			std::cout << "Enter path to image\n";
			std::getline(std::cin, path_to_image);
			break;
		case 2:
				{
				Measurement* measurement = new Measurement();
				resultsManager.add_new_measurement(measurement);
				measurement->set_paremeters("Sobel using OpenCV", Measurement::ComputationUnit::CPU, path_to_image);
				sobelObject.compute_cpu_opencv(measurement);
				}
				break;
		case 3:
				{
				Measurement* measurement = new Measurement();
				resultsManager.add_new_measurement(measurement);
				measurement->set_paremeters("Sobel own implementation", Measurement::ComputationUnit::CPU, path_to_image);
				sobelObject.compute_cpu_own(measurement);
				}
				break;
		case 4:
				{
				std::string temp;
				std::cout << "Enter number of work item in work group\n";
				std::getline(std::cin, temp);

				Measurement* measurement = new Measurement();
				resultsManager.add_new_measurement(measurement);
				measurement->set_paremeters("Sobel OpenCL", Measurement::ComputationUnit::GPU, path_to_image);
				measurement->set_work_items_size(std::stoi(temp));
				sobelObject.compute_gpu_opencl(measurement);

				}
				break;
		case 5:
				{
				Measurement* measurement = new Measurement();
				resultsManager.add_new_measurement(measurement);
				measurement->set_paremeters("Sobel continous using OpenCV", Measurement::ComputationUnit::CPU, path_to_image);
				sobelObject.compute_cpu_opencv_video(measurement);
				}
			break;
		case 6:
				{
				Measurement* measurement = new Measurement();
				resultsManager.add_new_measurement(measurement);
				measurement->set_paremeters("Sobel continous using OpenCL", Measurement::ComputationUnit::GPU, path_to_image);
				sobelObject.compute_gpu_opencl_video(measurement);
				}
				break;
		case 7:
				{
				std::string temp;
				std::cout << "Enter threshold value\n";
				std::getline(std::cin, temp);

				Measurement* measurement = new Measurement();
				resultsManager.add_new_measurement(measurement);
				measurement->set_paremeters("Threshold using OpenCV", Measurement::ComputationUnit::CPU, path_to_image);
				thresholdObject.compute_cpu_opencv(measurement, std::stoi(temp));
				}
				break;
		case 8:
				{
				std::string temp;
				std::cout << "Enter threshold value\n";
				std::getline(std::cin, temp);

				Measurement* measurement = new Measurement();
				resultsManager.add_new_measurement(measurement);
				measurement->set_paremeters("Threshold own implementation", Measurement::ComputationUnit::CPU, path_to_image);
				thresholdObject.compute_cpu_own(measurement, std::stoi(temp));
				}
				break;
		case 9:
				{
				std::string temp, th;
				std::cout << "Enter number of work item in work group\n";
				std::getline(std::cin, temp);

				std::cout << "Enter threshold value\n";
				std::getline(std::cin, th);

				Measurement* measurement = new Measurement();
				resultsManager.add_new_measurement(measurement);
				measurement->set_paremeters("Threshold OpenCL", Measurement::ComputationUnit::GPU, path_to_image);
				measurement->set_work_items_size(std::stoi(temp));
				thresholdObject.compute_gpu_opencl(measurement, std::stoi(th));
				}
				break;
		case 10:
				{
				Measurement* measurement = new Measurement();
				resultsManager.add_new_measurement(measurement);
				measurement->set_paremeters("Median using OpenCV", Measurement::ComputationUnit::CPU, path_to_image);
				medianFilter.compute_cpu_opencv(measurement);
				}
				break;
		case 11:
				{
				Measurement* measurement = new Measurement();
				resultsManager.add_new_measurement(measurement);
				measurement->set_paremeters("Median own implementation", Measurement::ComputationUnit::CPU, path_to_image);
				medianFilter.compute_cpu_own(measurement);
				}
				break;
		case 12:
				{
				std::string temp;
				std::cout << "Enter number of work item in work group\n";
				std::getline(std::cin, temp);

				Measurement* measurement = new Measurement();
				resultsManager.add_new_measurement(measurement);
				measurement->set_paremeters("Median OpenCL", Measurement::ComputationUnit::GPU, path_to_image);
				measurement->set_work_items_size(std::stoi(temp));
				medianFilter.compute_gpu_opencl(measurement);
				}
				break;
		case 13:
			resultsManager.print_results();
			break;
		case 14:
			resultsManager.save_results_to_files();
			break;
		}
	} while (key != 0);
	return 0;
}