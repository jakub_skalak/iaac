#include "SobelFilter.h"
#include "OpenClHelper.h"

#include "opencv2/core/core.hpp"
#include "opencv2/core/ocl.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS
#include <CL/cl.h>
#include <iostream>
#include <fstream>
#include <chrono>

using namespace std;
using namespace std::chrono;
using namespace cv;
using namespace std;

#include <opencv2/core/ocl.hpp>


SobelFilter::SobelFilter()
{

	
}

SobelFilter::SobelFilter(ResultsManager& manager, OpenCLHelper& helper)
{
	resultsManager = manager;
	openClHelper = helper;
}

void SobelFilter::compute_cpu_opencv(Measurement* measurement)
{
	cv::ocl::setUseOpenCL(false);

	//Creating matrix objects 
	Mat image, sobelx, sobely, sobel;

	//Read image from path and save to Mat object
	image = imread(measurement->get_path_to_image(), CV_LOAD_IMAGE_GRAYSCALE);

	//Sobel filtering
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	cv::Sobel(image, sobelx, CV_8U, 1, 0);
	cv::Sobel(image, sobely, CV_8U, 0, 1);
	cv::bitwise_or(sobelx, sobely, sobel);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	
	//Saving results in ms
	measurement->save_computation_time(duration_cast<nanoseconds>(t2 - t1).count() * 1e-6);

	//Uncomment to see output image
	/*imshow("Output image", sobel);
	waitKey();*/
}

//Referenced http://cis.k.hosei.ac.jp/~wakahara/sobel.c
void SobelFilter::compute_cpu_own(Measurement* measurement) {
	cv::ocl::setUseOpenCL(false);

	//Creating matrix objects 
	Mat image, dst;

	//Read image from path and save to Mat object
	image = imread(measurement->get_path_to_image(), IMREAD_GRAYSCALE);

	//Creating output matrix object with specified size and type
	dst = Mat(image.rows, image.cols, CV_8U);

	//Sobel mask
	int mask[3][3] = { { -1,  0,  1 },{ -2,  0,  2 },{ -1,  0,  1 }};

	double pixel_value;
	double min, max;

	/* Maximum values calculation after filtering*/
	min = DBL_MAX;
	max = -DBL_MAX;

	/* Initialization of destination image */
	for (int y = 0; y < image.rows; y++) {
		for (int x = 0; x < image.cols; x++) {
			dst.at<uchar>(y, x) = 0;
		}
	}

	//Sobel filtering
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	#pragma omp parallel for
	for (int y = 1; y < image.rows - 1; y++) {
		for (int x = 1; x < image.cols - 1; x++) {
			pixel_value = 0.0;
			for (int j = -1; j <= 1; j++) {
				for (int i = -1; i <= 1; i++) {
					pixel_value += mask[j + 1][i + 1] * image.at<uchar>(y + j, x + i);
				}
			}
			if (pixel_value < min) min = pixel_value;
			if (pixel_value > max) max = pixel_value;
			pixel_value = 255 * (pixel_value - min) / (max - min);
			dst.at<uchar>(y, x) = (unsigned char)pixel_value;
		}
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	//Saving results in ms
	measurement->save_computation_time(duration_cast<nanoseconds>(t2 - t1).count() * 1e-6);

//Uncomment to see output image
/*
imshow("Output image", dst);
waitKey();
*/
}

void SobelFilter::compute_gpu_opencl(Measurement* measurement) {

	//Creating OpenCL variable
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	cl_platform_id platform_id = NULL;
	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;
	cl_int ret;

	//Reading kernel from file
	std::fstream kernelFile("sobel.cl");
	std::string content(
		(std::istreambuf_iterator<char>(kernelFile)),
		std::istreambuf_iterator<char>()
	);

	//Setting content of kernel to kernelCharArray variable
	size_t csize = content.size();
	const char* kernelCharArray = new char[content.size()];
	kernelCharArray = content.c_str();

	/* Get Platform */
	ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Get Device Info */
	ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Create OpenCL context */
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Create Command Queue */
	command_queue = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Read image from path and save to Mat object
	cv::Mat image = cv::imread(measurement->get_path_to_image(), CV_LOAD_IMAGE_GRAYSCALE);
	char *buffer = reinterpret_cast<char *>(image.data);
	int width = image.cols;
	int height = image.rows;

	//Creating output matrix object with specified size and type
	cv::Mat out;
	out = cv::Mat(image.rows, image.cols, CV_8U);
	char *bufferOut = reinterpret_cast<char *>(image.data);
	
	//Creating image format for kernel use
	cl_image_format clImageFormat;
	clImageFormat.image_channel_order = CL_R;
	clImageFormat.image_channel_data_type = CL_UNORM_INT8;

	//Creating input image for kernel use
	cl_mem cl_image = clCreateImage2D(context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		&clImageFormat,
		width,
		height,
		0,
		buffer,
		&ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Offset
	size_t origin[] = { 0,0,0 };

	//Global work size
	size_t region[] = { width, height, 1 };

	//Send created input image to device(GPU) memory
	cl_event mem_send_event;
	ret = clEnqueueWriteImage(command_queue,
		cl_image,
		CL_TRUE,
		origin,
		region,
		0,
		0,
		buffer,
		0, 0, &mem_send_event);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Wait for send memory, then save duration 
	clWaitForEvents(1, &mem_send_event);
	clFinish(command_queue);
	measurement->save_send_to_gpu_time(openClHelper.get_event_exec_time(mem_send_event));

	//Creating output image for kernel use
	cl_mem cl_image_dest = clCreateImage2D(context,
		CL_MEM_WRITE_ONLY,
		&clImageFormat,
		width,
		height,
		0,
		bufferOut,
		&ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;


	//Creating program with kernel source
	program = clCreateProgramWithSource(context, 1, (const char **)&kernelCharArray,
		(const size_t *)&csize, &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Build Kernel Program */
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Create OpenCL Kernel */
	kernel = clCreateKernel(program, "sobel", &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Set OpenCL Kernel Parameters */
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&cl_image);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;
	ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&cl_image_dest);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Creating array with size of work units
	int fixed_work_items_for_rows = (height % measurement->get_work_items_size() == 0 ? measurement->get_work_items_size() : measurement->get_work_items_size() - 1);
	size_t local_threads[] = { measurement->get_work_items_size(), fixed_work_items_for_rows, 1};

	//Execute computation on GPU
	cl_event exec_event;
	ret = clEnqueueNDRangeKernel(command_queue,
		kernel,
		2,
		0,
		region,
		local_threads,
		0, 0, &exec_event);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Wait for computation results, then save duration
	clWaitForEvents(1, &exec_event);
	clFinish(command_queue);
	measurement->save_computation_time(openClHelper.get_event_exec_time(exec_event));

	//Download memory from GPU to host
	cl_event memory_download_event;
	ret = clEnqueueReadImage(command_queue,
		cl_image_dest,
		CL_TRUE,
		origin,
		region,
		0,
		0,
		bufferOut,
		0, 0, &memory_download_event);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Measure time of download and save
	clWaitForEvents(1, &memory_download_event);
	clFinish(command_queue);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;
	measurement->save_download_time(openClHelper.get_event_exec_time(memory_download_event));


//Uncomment to see result image
/*	cv::Mat output_image = cv::Mat(height, width, CV_8U, bufferOut);
	imshow("Output image", output_image);
	waitKey();*/

	//Releasing OpenCL objects 
	ret = clFlush(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseMemObject(cl_image_dest);
	ret = clReleaseMemObject(cl_image);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Releasing OpenCV Mat objects 
	image.release();
	out.release();
}

//Continuous reading image and filtering using OpenCV
void SobelFilter::compute_cpu_opencv_video(Measurement* measurement) 
{
	cv::ocl::setUseOpenCL(false);

	Mat image, sobelx, sobely;

	char c;
	double total_time = 0.0;
	int cpt = 0;
	do
	{		
		image = cv::imread(measurement->get_path_to_image(), CV_LOAD_IMAGE_GRAYSCALE);
		
		double t = (double)cv::getTickCount();
		cv::Sobel(image, sobelx, image.depth(), 1, 0, 3);
		cv::Sobel(image, sobely, image.depth(), 0, 1, 3);

		cv::bitwise_or(sobelx, sobely, image);
		t = ((double)cv::getTickCount() - t) / cv::getTickFrequency();
		total_time += t;
		cpt++;
		std::cout << "Duration: " << t << "[s]; FPS: " << (1 / t) << " ; average FPS= " << (cpt / total_time) << std::endl;

		cv::imshow("Output image", image);

		c = cv::waitKey(27);
	} while (c != 27);

}

//Continuous reading image and filtering using OpenCL
void SobelFilter::compute_gpu_opencl_video(Measurement* measurement) 
{

	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	cl_platform_id platform_id = NULL;
	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;
	cl_int ret;
	int errNum;

	std::fstream kernelFile("sobel.cl");
	std::string content(
		(std::istreambuf_iterator<char>(kernelFile)),
		std::istreambuf_iterator<char>()
	);

	size_t csize = content.size();
	const char* kernelCharArray = new char[content.size()];
	kernelCharArray = content.c_str();

	/* Get Platform and Device Info */
	ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;
	ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Create OpenCL context */
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Create Command Queue */
	command_queue = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	Mat image, frame, out;

	cl_image_format cl_img_format;
	cl_img_format.image_channel_order = CL_R;
	cl_img_format.image_channel_data_type = CL_UNORM_INT8;

	program = clCreateProgramWithSource(context, 1, (const char **)&kernelCharArray,
		(const size_t *)&csize, &ret);

	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Build Kernel Program */
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Create OpenCL Kernel */
	kernel = clCreateKernel(program, "sobel", &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	size_t origin[] = { 0,0,0 };

	char c;
	double total_time = 0.0;
	int cpt = 0;
	do
	{

		cv::imread(measurement->get_path_to_image(), CV_LOAD_IMAGE_GRAYSCALE).copyTo(image);
		out = cv::Mat(image.rows, image.cols, CV_8U);
		char *bufferOut = reinterpret_cast<char *>(out.data);

		int width = image.cols;
		int height = image.rows;
		size_t region[] = { width, height, 1 };

		char *buffer = reinterpret_cast<char *>(image.data);

		double t = (double)cv::getTickCount();
		cl_mem clImage = clCreateImage2D(context,
			CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
			&cl_img_format,
			width,
			height,
			0,
			buffer,
			&errNum);
	
		ret = clEnqueueWriteImage(command_queue,
			clImage,
			CL_TRUE,
			origin,
			region,
			0,
			0,
			buffer,
			0, 0, 0);
		
		cl_mem clImageDest = clCreateImage2D(context,
			CL_MEM_WRITE_ONLY,
			&cl_img_format,
			width,
			height,
			0,
			bufferOut,
			&errNum);

		/* Set OpenCL Kernel Parameters */
		ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&clImage);
		ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&clImageDest);	

		//Creating array with size of work units
		int fixed_work_items_for_rows = (height % measurement->get_work_items_size() == 0 ? measurement->get_work_items_size() : measurement->get_work_items_size() - 1);
		size_t local_threads[] = { measurement->get_work_items_size(), fixed_work_items_for_rows, 1 };

		ret = clEnqueueNDRangeKernel(command_queue,
			kernel,
			3,
			0,
			region,
			local_threads,
			0, 0, 0);

		ret = clEnqueueReadImage(command_queue,
			clImageDest,
			CL_TRUE,
			origin,
			region,
			0,
			0,
			bufferOut,
			0, 0, 0);

		ret = clFlush(command_queue);
		t = ((double)cv::getTickCount() - t) / cv::getTickFrequency();

		cv::Mat output_image = cv::Mat(height, width, CV_8U, bufferOut);

		ret = clFinish(command_queue);
		ret = clReleaseMemObject(clImageDest);
		ret = clReleaseMemObject(clImage);

		total_time += t;
		cpt++;
		std::cout << "Duration: " << t << "[s] ; FPS: " << (1 / t) << " ; average FPS=" << (cpt / total_time) << std::endl;

		cv::imshow("Output image", output_image);
		image.deallocate();
		c = cv::waitKey(30);
	} while (c != 27);

	ret = clFlush(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);
}

SobelFilter::~SobelFilter()
{
}
