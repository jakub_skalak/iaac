#pragma once

#include "ResultsManager.h"
#include "OpenCLHelper.h"

#include <chrono>

class SobelFilter {

private:
	ResultsManager resultsManager;
	OpenCLHelper openClHelper;
public:
	SobelFilter();
	SobelFilter(ResultsManager& resultsManager, OpenCLHelper& openClHelper);
	void compute_cpu_opencv(Measurement* measurement);
	void compute_cpu_own(Measurement* measurement);
	void compute_gpu_opencl(Measurement* measurement);
	void compute_cpu_opencv_video(Measurement* measurement);
	void compute_gpu_opencl_video(Measurement* measurement);
	~SobelFilter();
};

