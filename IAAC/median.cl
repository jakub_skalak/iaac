__constant sampler_t sampler = CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

__kernel void median(__read_only image2d_t input_image, __write_only image2d_t output_image)
{
	unsigned int i = get_global_id(0);
	unsigned int j = get_global_id(1);
	float4 values[9];
	float temp_value;

	if (i >= get_image_width(input_image) || j >= get_image_height(input_image))
		return;

	//Get pixels value
	values[0] = read_imagef(input_image, sampler, (int2)(i - 1, j - 1));
	values[1] = read_imagef(input_image, sampler, (int2)(i, j - 1));
	values[2] = read_imagef(input_image, sampler, (int2)(i + 1, j - 1));
	values[3] = read_imagef(input_image, sampler, (int2)(i - 1, j));
	values[4] = read_imagef(input_image, sampler, (int2)(i + 1, j));
	values[5] = read_imagef(input_image, sampler, (int2)(i - 1, j + 1));
	values[6] = read_imagef(input_image, sampler, (int2)(i, j + 1));
	values[7] = read_imagef(input_image, sampler, (int2)(i + 1, j + 1));
	values[8] = read_imagef(input_image, sampler, (int2)(i, j));

	//Simple insertion sort to sort values in growing order, median is under index nr 4
	int x, y;
	for (x = 0; x < 9; ++x) {
		temp_value = values[x].x;
		for (y = x - 1; y >= 0 && temp_value < values[y].x; --y) {
			values[y + 1].x = values[y].x;
		}
		values[y + 1].x = temp_value;
	}

	//Write output pixel value
	write_imagef(output_image, (int2)(i, j), values[4].x);
}