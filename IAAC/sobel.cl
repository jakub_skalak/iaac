__constant sampler_t sampler = CLK_ADDRESS_CLAMP_TO_EDGE |CLK_FILTER_NEAREST;

//Referenced book OpenCL Programming Guide, Aaftab Munshi, Addison-Wesley, 2011
kernel void sobel(read_only image2d_t input_image, write_only image2d_t output_image)
{
	int i = (int)get_global_id(0);
	int j = (int)get_global_id(1);

	if (i >= get_image_width(input_image) || j >= get_image_height(input_image))
		return;

	//Get pixels value
	float4 p00 = read_imagef(input_image, sampler, (int2)(i - 1, j - 1));
	float4 p10 = read_imagef(input_image, sampler, (int2)(i, j - 1));
	float4 p20 = read_imagef(input_image, sampler, (int2)(i + 1, j - 1));
	float4 p01 = read_imagef(input_image, sampler, (int2)(i - 1, j));
	float4 p21 = read_imagef(input_image, sampler, (int2)(i + 1, j));
	float4 p02 = read_imagef(input_image, sampler, (int2)(i - 1, j + 1));
	float4 p12 = read_imagef(input_image, sampler, (int2)(i, j + 1));
	float4 p22 = read_imagef(input_image, sampler, (int2)(i + 1, j + 1));

	//Calculate pixel final value
	float3 gx = -p00.xyz + p20.xyz + 2.0f * (p21.xyz - p01.xyz) - p02.xyz + p22.xyz;
	float3 gy = -p00.xyz - p20.xyz + 2.0f * (p12.xyz - p10.xyz) + p02.xyz + p22.xyz;
	float gs_x = 0.3333f * (gx.x + gx.y + gx.z);
	float gs_y = 0.3333f * (gy.x + gy.y + gy.z);
	float g = native_sqrt(gs_x * gs_x + gs_y * gs_y); 	//Gradient magnitude

	//Write output pixel value
	write_imagef(output_image, (int2)(i, j), (float4)(g, g, g, 1.0f));
}

