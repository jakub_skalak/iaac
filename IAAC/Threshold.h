#pragma once

#include "ResultsManager.h"
#include "OpenCLHelper.h"

#include <chrono>

class Threshold
{

private:
	ResultsManager resultsManager;
	OpenCLHelper openClHelper;
public:
	Threshold();
	Threshold(ResultsManager& resultsManager, OpenCLHelper& openClHelper);
	void compute_cpu_opencv(Measurement* measurement, int threshold);
	void compute_cpu_own(Measurement* measurement, int threshold);
	void compute_gpu_opencl(Measurement* measurement, int threshold);
	~Threshold();
};

