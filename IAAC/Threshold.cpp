#include "Threshold.h"

#include "opencv2/core/core.hpp"
#include "opencv2/core/ocl.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS
#include <CL/cl.h>
#include <iostream>
#include <fstream>
#include <chrono>

using namespace std;
using namespace std::chrono;
using namespace cv;
using namespace std;

#include <opencv2/core/ocl.hpp>


Threshold::Threshold()
{}

Threshold::Threshold(ResultsManager& manager, OpenCLHelper& helper)
{
	resultsManager = manager;
	openClHelper = helper;
}

void Threshold::compute_cpu_opencv(Measurement* measurement, int th) 
{
	cv::ocl::setUseOpenCL(false);

	//Creating matrix objects 
	Mat image, im_threshold;
	//Read image from path and save to Mat object
	image = imread(measurement->get_path_to_image(), IMREAD_GRAYSCALE);

	//Threshold
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	threshold(image, im_threshold, th, 255.0, THRESH_BINARY);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	
	//Saving results in ms
	measurement->save_computation_time(duration_cast<nanoseconds>(t2 - t1).count() * 1e-6);

	//Uncomment to see output image
	/*imshow("Output image", im_threshold);
	waitKey();*/
}

void Threshold::compute_cpu_own(Measurement* measurement, int threshold) {
	cv::ocl::setUseOpenCL(false);

	//Creating matrix objects 
	Mat image, dst;

	//Read image from path and save to Mat object
	image = imread(measurement->get_path_to_image(), IMREAD_GRAYSCALE);

	//Creating output matrix object with specified size and type
	dst = Mat(image.rows, image.cols, CV_8U);

	//Treshold
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	#pragma omp parallel for
	for (int x = 0; x < image.rows; ++x) {
		for (int y = 0; y < image.cols; ++y) {
			if (image.at<uchar>(x, y) > threshold) {
				dst.at<uchar>(x, y) = 255;
			}
			else {
				dst.at<uchar>(x, y) = 0;
			}
		}
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	
	//Saving results in ms
	measurement->save_computation_time(duration_cast<nanoseconds>(t2 - t1).count() * 1e-6);
	
	//Uncomment to see output image
	/*
	imshow("Output image", dst);
	waitKey();
	*/
}

void Threshold::compute_gpu_opencl(Measurement* measurement, int threshold) {
	
	//Creating OpenCL variable
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	cl_platform_id platform_id = NULL;
	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;
	cl_int ret;
	int errNum;

	//Reading kernel from file
	std::fstream kernelFile("threshold.cl");
	std::string content(
		(std::istreambuf_iterator<char>(kernelFile)),
		std::istreambuf_iterator<char>()
	);

	//Setting content of kernel to kernelCharArray variable
	size_t csize = content.size();
	const char* kernelCharArray = new char[content.size()];
	kernelCharArray = content.c_str();

	/* Get Platform */
	ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Get Device Info */
	ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Create OpenCL context */
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Create Command Queue */
	command_queue = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Read image from path and save to Mat object
	cv::Mat image = cv::imread(measurement->get_path_to_image(), CV_LOAD_IMAGE_GRAYSCALE);
	char *buffer = reinterpret_cast<char *>(image.data);
	int width = image.cols;
	int height = image.rows;

	//Creating output matrix object with specified size and type
	cv::Mat out;
	out = cv::Mat(image.rows, image.cols, CV_8U);
	char *bufferOut = reinterpret_cast<char *>(image.data);

	//Creating image format for kernel use
	cl_image_format cl_img_format;
	cl_img_format.image_channel_order = CL_R;
	cl_img_format.image_channel_data_type = CL_UNORM_INT8;

	//Creating input image for kernel use
	cl_mem cl_image = clCreateImage2D(context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		&cl_img_format,
		width,
		height,
		0,
		buffer,
		&errNum);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Offset
	size_t origin[] = { 0,0,0 };

	//Global work size
	size_t region[] = { width, height, 1 };

	//Send created input image to device(GPU) memory
	cl_event mem_send_event;
	ret = clEnqueueWriteImage(command_queue,
		cl_image,
		CL_TRUE,
		origin,
		region,
		0,
		0,
		buffer,
		0, 0, &mem_send_event);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Wait for send memory, then save duration
	clWaitForEvents(1, &mem_send_event);
	clFinish(command_queue);
	measurement->save_send_to_gpu_time(openClHelper.get_event_exec_time(mem_send_event));

	//Creating output image for kernel use
	cl_mem cl_image_dest = clCreateImage2D(context,
		CL_MEM_WRITE_ONLY,
		&cl_img_format,
		width,
		height,
		0,
		bufferOut,
		&errNum);
	if (errNum != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(errNum) << std::endl;

	//Creating program with kernel source
	program = clCreateProgramWithSource(context, 1, (const char **)&kernelCharArray,
		(const size_t *)&csize, &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Build Kernel Program */
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Create OpenCL Kernel */
	kernel = clCreateKernel(program, "threshold", &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Set OpenCL Kernel Parameters */
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&cl_image);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;
	ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&cl_image_dest);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;
	ret = clSetKernelArg(kernel, 2, sizeof(int), (void *)&threshold);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Creating array with size of work units
	int fixed_rows = (height % measurement->get_work_items_size() == 0 ? measurement->get_work_items_size() : measurement->get_work_items_size() - 1);
	size_t localThreads[] = { measurement->get_work_items_size(), fixed_rows, 1 };

	//Execute computation on GPU
	cl_event execEvent;
	ret = clEnqueueNDRangeKernel(command_queue,
		kernel,
		2,
		0,
		region,
		localThreads,
		0, 0, &execEvent);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Wait for computation results, then save duration
	clWaitForEvents(1, &execEvent);
	clFinish(command_queue);
	measurement->save_computation_time(openClHelper.get_event_exec_time(execEvent));

	//Download memory from GPU to host
	cl_event memDownloadEvent;
	ret = clEnqueueReadImage(command_queue,
		cl_image_dest,
		CL_TRUE,
		origin,
		region,
		0,
		0,
		bufferOut,
		0, 0, &memDownloadEvent);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Measure time of download and save
	clWaitForEvents(1, &memDownloadEvent);
	clFinish(command_queue);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;
	measurement->save_download_time(openClHelper.get_event_exec_time(memDownloadEvent));


	//Uncomment to see result image
	/*	cv::Mat output_image = cv::Mat(height, width, CV_8U, bufferOut);
	imshow("Output image", output_image);
	waitKey();*/

	//Releasing OpenCL objects 
	ret = clFlush(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseMemObject(cl_image_dest);
	ret = clReleaseMemObject(cl_image);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Releasing OpenCV Mat objects 
	image.release();
	out.release();
}

Threshold::~Threshold()
{
}
