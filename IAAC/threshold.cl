__constant sampler_t sampler = CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

__kernel void threshold(__read_only image2d_t input_image, __write_only image2d_t output_image, int th)
{
	int i = (int)get_global_id(0);
	int j = (int)get_global_id(1);

	if (i >= get_image_width(input_image) || j >= get_image_height(input_image))
		return;

	//Get input pixel value
	float4 pixel_value = read_imagef(input_image, sampler, (int2)(i, j));

	//Set output pixel value
	if (pixel_value.x > th / 255.0) {
		write_imagef(output_image, (int2)(i, j), 255.);
	}
	else {
		write_imagef(output_image, (int2)(i, j), 0.);
	}
}