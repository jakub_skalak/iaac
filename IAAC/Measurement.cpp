#include "Measurement.h"

#include <iostream>
#include <fstream>

Measurement::Measurement()
{
}

void Measurement::set_paremeters(std::string an, ComputationUnit cu, std::string path)
{
	alghoritm_name = an;
	computationUnit = cu;
	path_to_image = path;
}

void Measurement::print_parameters()
{
	std::cout << "Alghoritm name: " + alghoritm_name << std::endl;
	std::string unit = computationUnit == CPU ? "CPU" : "GPU";
	std::cout << "Computation unit: " + unit << std::endl;
	std::cout << "Measurement time: " + std::to_string(measurement_time) << std::endl;
	if (computationUnit == GPU ? true : false) {
		std::cout << "Work items in work group: " + std::to_string(work_items_in_work_group) << std::endl;
		std::cout << "Send to GPU time: " + std::to_string(send_to_gpu_time) << std::endl;
		std::cout << "Download from GPU time: " + std::to_string(download_from_gpu_time) << std::endl;
	}
	std::cout << std::endl;
}

void Measurement::save_computation_time(double elapsed_time)
{
	measurement_time = elapsed_time;
}

void Measurement::save_send_to_gpu_time(double elapsed_time)
{
	send_to_gpu_time = elapsed_time;
}

void Measurement::save_download_time(double elapsed_time)
{
	download_from_gpu_time = elapsed_time;
}

void Measurement::set_work_items_size(int size)
{
	work_items_in_work_group = size;
}

int Measurement::get_work_items_size()
{
	return work_items_in_work_group;
}

std::string Measurement::get_path_to_image()
{
	return path_to_image;
}

void Measurement::save_results_to_file()
{
	if (computationUnit == GPU) {
		std::ofstream ocl_file;
		ocl_file.open("opencl_results.txt", std::ios::out | std::ios::app);
		ocl_file << std::to_string(send_to_gpu_time) << "\t" << std::to_string(measurement_time) << "\t" << std::to_string(send_to_gpu_time) << "\n";
		ocl_file.close();
	}
	else {
		std::ofstream cpu_file;
		cpu_file.open("cpu_results.txt", std::ios::out | std::ios::app);
		cpu_file << std::to_string(measurement_time) << "\n";
		cpu_file.close();
	}

}


Measurement::~Measurement()
{
}
