#include "ResultsManager.h"

#include <iostream>

ResultsManager::ResultsManager()
{
}

void ResultsManager::add_new_measurement(Measurement* measurement)
{
	measurements.push_back(measurement);
}

void ResultsManager::save_results_to_files()
{
	for each (Measurement* m in measurements)
	{
		m->save_results_to_file();
	}
}



void ResultsManager::print_results()
{
	for each (Measurement* m in measurements)
	{
		m->print_parameters();
	}
}


ResultsManager::~ResultsManager()
{
}
