#pragma once

#include<vector>

#include"Measurement.h"

class ResultsManager
{
private:
	std::vector<Measurement*> measurements;
public:
	ResultsManager();
	void add_new_measurement(Measurement* measurement);
	void save_results_to_files();
	void print_results();
	~ResultsManager();
};

