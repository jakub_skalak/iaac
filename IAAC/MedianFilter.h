#pragma once

#include "ResultsManager.h"
#include "OpenCLHelper.h"

#include <chrono>

class MedianFilter {

private:
	ResultsManager resultsManager;
	OpenCLHelper openClHelper;
public:
	MedianFilter();
	MedianFilter(ResultsManager& resultsManager, OpenCLHelper& openClHelper);
	void compute_cpu_opencv(Measurement* measurement);
	void compute_cpu_own(Measurement* measurement);
	void compute_gpu_opencl(Measurement* measurement);
	~MedianFilter();
};

