#include "MedianFilter.h"
#include "OpenClHelper.h"

#include "opencv2/core/core.hpp"
#include "opencv2/core/ocl.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS
#include <CL/cl.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>

using namespace std;
using namespace std::chrono;
using namespace cv;
using namespace std;

#include <opencv2/core/ocl.hpp>


MedianFilter::MedianFilter()
{


}

MedianFilter::MedianFilter(ResultsManager& manager, OpenCLHelper& helper)
{
	resultsManager = manager;
	openClHelper = helper;
}

//https://stackoverflow.com/questions/20473352/is-conversion-to-gray-scale-a-necessary-step-in-image-preprocessing
void MedianFilter::compute_cpu_opencv(Measurement* measurement) // zamienic na do_measurements_on_cpu
{
	cv::ocl::setUseOpenCL(false);

	//Creating matrix objects 
	Mat image, median;

	//Read image from path and save to Mat object
	image = imread(measurement->get_path_to_image(), CV_LOAD_IMAGE_GRAYSCALE);

	//Median filtering
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	medianBlur(image, median, 3);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	
	//Saving results in ms
	measurement->save_computation_time(duration_cast<nanoseconds>(t2 - t1).count() * 1e-6);

	//Uncomment to see output image
	/*imshow("Output image", median);
	waitKey();*/
}

//http://www.programming-techniques.com/2013/02/median-filter-using-c-and-opencv-image.html
void MedianFilter::compute_cpu_own(Measurement* measurement) {
	cv::ocl::setUseOpenCL(false);

	//Creating matrix objects 
	Mat image, dst;

	//Read image from path and save to Mat object
	image = imread(measurement->get_path_to_image(), IMREAD_GRAYSCALE);
	
	//Creating output matrix object with specified size and type
	dst = Mat(image.rows, image.cols, CV_8U);

	/* Initialization of destination image */
	for (int y = 0; y < image.rows; y++) {
		for (int x = 0; x < image.cols; x++) {
			dst.at<uchar>(y, x) = 0;
		}
	}

	int window[9];

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
//#pragma omp parallel for
	for (int y = 1; y < image.rows - 1; y++) {
		for (int x = 1; x < image.cols - 1; x++) {

			//Saving mask values to array
			window[0] = image.at<uchar>(y - 1, x - 1);
			window[1] = image.at<uchar>(y, x - 1);
			window[2] = image.at<uchar>(y + 1, x - 1);
			window[3] = image.at<uchar>(y - 1, x);
			window[4] = image.at<uchar>(y, x);
			window[5] = image.at<uchar>(y + 1, x);
			window[6] = image.at<uchar>(y - 1, x + 1);
			window[7] = image.at<uchar>(y, x + 1);
			window[8] = image.at<uchar>(y + 1, x + 1);

			//Sort the window to find median
			std::sort(std::begin(window), std::end(window));

			//Sssign the median to centered element of the matrix
			dst.at<uchar>(y, x) = window[4];
		}
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	
	//Saving results in ms
	measurement->save_computation_time(duration_cast<nanoseconds>(t2 - t1).count() * 1e-6);

	//Uncomment to see output image
	/*
	imshow("Output image", dst);
	waitKey();
	*/
}

void MedianFilter::compute_gpu_opencl(Measurement* measurement) {

	//Creating OpenCL variable	
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	cl_platform_id platform_id = NULL;
	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;
	cl_int ret;
	int errNum;

	//Reading kernel from file
	std::fstream kernelFile("median.cl");
	std::string content(
		(std::istreambuf_iterator<char>(kernelFile)),
		std::istreambuf_iterator<char>()
	);

	//Setting content of kernel to kernelCharArray variable
	size_t csize = content.size();
	const char* kernelCharArray = new char[content.size()];
	kernelCharArray = content.c_str();

	/* Get Platform */
	ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Get Device Info */
	ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Create OpenCL context */
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Create Command Queue */
	command_queue = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Read image from path and save to Mat object
	cv::Mat image = cv::imread(measurement->get_path_to_image(), CV_LOAD_IMAGE_GRAYSCALE);
	char *buffer = reinterpret_cast<char *>(image.data);
	int width = image.cols;
	int height = image.rows;

	//Creating output matrix object with specified size and type
	cv::Mat out;
	out = cv::Mat(image.rows, image.cols, CV_8U);
	char *bufferOut = reinterpret_cast<char *>(image.data);

	//Creating image format for kernel use
	cl_image_format cl_img_format;
	cl_img_format.image_channel_order = CL_R;
	cl_img_format.image_channel_data_type = CL_UNORM_INT8;

	//Creating input image for kernel use
	cl_mem cl_image = clCreateImage2D(context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		&cl_img_format,
		width,
		height,
		0,
		buffer,
		&errNum);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Offset
	size_t origin[] = { 0,0,0 };

	//Global work size
	size_t region[] = { width, height, 1 };

	//Send created input image to device(GPU) memory
	cl_event mem_send_event;
	ret = clEnqueueWriteImage(command_queue,
		cl_image,
		CL_TRUE,
		origin,
		region,
		0,
		0,
		buffer,
		0, 0, &mem_send_event);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;


	//Wait for send memory, then save duration 
	clWaitForEvents(1, &mem_send_event);
	clFinish(command_queue);
	measurement->save_send_to_gpu_time(openClHelper.get_event_exec_time(mem_send_event));

	//Creating output image for kernel use
	cl_mem cl_image_dest = clCreateImage2D(context,
		CL_MEM_WRITE_ONLY,
		&cl_img_format,
		width,
		height,
		0,
		bufferOut,
		&errNum);
	if (errNum != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(errNum) << std::endl;

	//Creating program with kernel source
	program = clCreateProgramWithSource(context, 1, (const char **)&kernelCharArray,
		(const size_t *)&csize, &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Build Kernel Program */
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Create OpenCL Kernel */
	kernel = clCreateKernel(program, "median", &ret);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	/* Set OpenCL Kernel Parameters */
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&cl_image);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;
	ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&cl_image_dest);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Creating array with size of work units
	int fixed_rows = (height % measurement->get_work_items_size() == 0 ? measurement->get_work_items_size() : measurement->get_work_items_size() - 1);
	size_t localThreads[] = { measurement->get_work_items_size(), fixed_rows, 1 };

	//Execute computation on GPU
	cl_event execEvent;
	ret = clEnqueueNDRangeKernel(command_queue,
		kernel,
		2,
		0,
		region,
		localThreads,
		0, 0, &execEvent);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Wait for computation results, then save duration
	clWaitForEvents(1, &execEvent);
	clFinish(command_queue);
	measurement->save_computation_time(openClHelper.get_event_exec_time(execEvent));

	//Download memory from GPU to host
	cl_event memDownloadEvent;
	ret = clEnqueueReadImage(command_queue,
		cl_image_dest,
		CL_TRUE,
		origin,
		region,
		0,
		0,
		bufferOut,
		0, 0, &memDownloadEvent);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Measure time of download and save
	clWaitForEvents(1, &memDownloadEvent);
	clFinish(command_queue);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;
	measurement->save_download_time(openClHelper.get_event_exec_time(memDownloadEvent));

	//Uncomment to see result image
	/*	cv::Mat output_image = cv::Mat(height, width, CV_8U, bufferOut);
	imshow("Output image", output_image);
	waitKey();*/

	//Releasing OpenCL objects 
	ret = clFlush(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseMemObject(cl_image_dest);
	ret = clReleaseMemObject(cl_image);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);
	ret = clReleaseEvent(execEvent);
	ret = clReleaseEvent(memDownloadEvent);
	ret = clReleaseEvent(mem_send_event);
	if (ret != CL_SUCCESS)
		std::cerr << openClHelper.get_error_string(ret) << std::endl;

	//Releasing OpenCV Mat objects 
	image.release();
	out.release();
}

MedianFilter::~MedianFilter()
{
}
